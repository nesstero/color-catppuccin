#!/usr/bin/python
from pyrof.rofi import Rofi
import subprocess as sub
from os import path as p

class color:

    def __init__(self):
        self.color = {
                "[1]  Flamingo": "#F2CDCD",
                "[2]  Mauve": "#DDB6F2",
                "[3]  Pink": "#F5C2E7",
                "[4]  Maroon": "#E8A2AF",
                "[5]  Red": "#F28FAD",
                "[6]  Peach": "#F8BD96",
                "[7]  Yellow": "#F8BD96",
                "[8]  Green": "#ABE9B3",
                "[9]  Teal": "#B5E8E0",
                "[10] Blue": "#96CDFB",
                "[11] Sky": "#89DCEB",
                "[1]  Black 0": "#161320",
                "[2]  Black 1": "#1A1826",
                "[3]  Black 2": "#1E1E2E",
                "[4]  Black 3": "#302D41",
                "[5]  Black 4": "#575268",
                "[6]  Gray 0": "#6E6C7E",
                "[7]  Gray 1": "#988BA2",
                "[8]  Gray 2": "#C3BAC6",
                "[9]  White": "#D9E0EE",
                "[10] Lavender": "#C9CBFF",
                "[11] Rosewater": "#F5E0DC"
                }
        self.dr = p.dirname(p.abspath(__file__))
        back = p.join(self.dr, "back.png")
        self.th = '* {background-color: transparent;} listview {lines: 11; columns: 2;} window {width: 500px;} message {background-image: url("' + back + '", none);}'

    def copy_color(self):
        menu = Rofi(theme_str=self.th)
        menu.case_insensitive = True
        menu.prompt = "Copy Color Catppuccin"
        menu.mesg = """




        """
        menu = menu(self.color)
        color_selected = menu.value
        cp_color = sub.Popen(['xclip', '-selection', 'clipboard'], stdin=sub.PIPE, close_fds=True)
        cp_color.communicate(input=color_selected.encode("utf-8"))

cp_color = color()
cp_color.copy_color()
